#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define ZIP_MAGIC_HEADER "\x50\x4B\x03\x04"

#pragma pack(push, 1)
struct ZipLocalFileHeader {
    uint32_t signature;
    uint16_t versionNeeded;
    uint16_t flags;
    uint16_t compressionMethod;
    uint16_t lastModFileTime;
    uint16_t lastModFileDate;
    uint32_t crc32;
    uint32_t compressedSize;
    uint32_t uncompressedSize;
    uint16_t fileNameLength;
    uint16_t extraFieldLength;
};

//Функция проверки файла на rarjpeg
int is_rarjpeg(const char *filename) {
// Открытие файла на чтение в бинарном режиме
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        perror("fopen");
        return 0;
    }

  printf("In rarjpeg\n");

  char header[4];
  int i, sz;
  //Опеределение размера файла
  fseek(file, 0L, SEEK_END);
  sz = ftell(file);
  //Поиск заголовка zip со смещением по 4 байта
  fseek(file, 0, SEEK_SET);
  for (i = 1; i < sz; i++) {
    fseek(file, i, SEEK_SET);
    fread(header, 4, 1, file);
    if (memcmp(header, ZIP_MAGIC_HEADER, 4) == 0) {
        return i;
    }
  }

return 0;
}

void list_rar_files(const char *filename, int position) {
    FILE *file = fopen(filename, "rb");
    if (!file) {
        perror("Ошибка при открытии файла");
        return;
    }

        fseek(file, 0, SEEK_END);
        long fileSize = ftell(file);
        fseek(file, position, SEEK_SET);

        while (ftell(file) < fileSize) {
            struct ZipLocalFileHeader header;
            fread(&header, sizeof(header), 1, file);

            if (header.signature == 0x04034b50) { // Signature of a local file header
                printf("Good\n");
                char fileName[256];
                fread(fileName, header.fileNameLength, 1, file);
                fileName[header.fileNameLength] = '\0';
                printf("File: %s\n", fileName);
                fseek(file, header.extraFieldLength, SEEK_CUR);
                fseek(file, header.compressedSize, SEEK_CUR);
            } else {
                break;
            }
        }
        fclose(file);    
}


int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("Использование: %s <имя файла>\n", argv[0]);
        return 1;
    }

    const char *filename = argv[1];

    int zip_position;
    if ((zip_position=is_rarjpeg(filename))) {
        printf("Файл '%s' является Rarjpeg.\nСодержимое RAR архива:\n", filename);
        list_rar_files(filename, zip_position);
    } else {
        printf("Файл '%s' не является Rarjpeg.\n", filename);
    }

    return 0;
}

#pragma pack(pop)
